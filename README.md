# Cake Manager

Cake manager application for Waracle.

## To run the application locally

```
.\mvnw clean spring-boot:run
```

Service will be deployed in 8080 port Alternatively can use Open API (swagger) ui at
[http://localhost:8080/swagger-ui/index.html](http://localhost:8080/swagger-ui/index.html)

Tech stack is updated with jdk 17 and spring boot 2.6.2

## Other features

- Gitlab CI can be
  accessed [https://gitlab.com/dsamarasekara/cake-manager](https://gitlab.com/dsamarasekara/cake-manager)
- CI includes building the docker image with jib maven plugin
- 
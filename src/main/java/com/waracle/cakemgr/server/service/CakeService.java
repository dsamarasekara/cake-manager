package com.waracle.cakemgr.server.service;

import com.waracle.cakemgr.dto.CakeDTO;
import com.waracle.cakemgr.server.model.CakeEntity;
import com.waracle.cakemgr.server.repository.CakeRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CakeService {

    private final CakeRepository cakeRepository;

    public CakeService(CakeRepository cakeRepository) {
        this.cakeRepository = cakeRepository;
    }

    public List<CakeDTO> findAll() {
        List<CakeEntity> cakeEntities = cakeRepository.findAll();
        return cakeEntities
                .stream()
                .map(CakeService::convertToDto)
                .toList();
    }

    public CakeDTO create(CakeDTO cakeDTO) {
        CakeEntity cakeEntity = convertToEntity(cakeDTO);
        CakeEntity savedCakeEntity = cakeRepository.save(cakeEntity);
        return convertToDto(savedCakeEntity);
    }

    private static CakeEntity convertToEntity(CakeDTO cakeDTO) {
        CakeEntity cakeEntity = new CakeEntity();
        BeanUtils.copyProperties(cakeDTO, cakeEntity);
        return cakeEntity;
    }

    private static CakeDTO convertToDto(CakeEntity cakeEntity) {
        CakeDTO cakeDTO = new CakeDTO();
        BeanUtils.copyProperties(cakeEntity, cakeDTO);
        return cakeDTO;
    }
}

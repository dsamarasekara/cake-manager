package com.waracle.cakemgr.server.api;

import com.waracle.cakemgr.dto.CakeDTO;
import com.waracle.cakemgr.dto.ErrorDTO;
import com.waracle.cakemgr.server.service.CakeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

import static com.waracle.cakemgr.dto.ErrorDTO.ErrorCode.DUPLICATE_TITLE;
import static com.waracle.cakemgr.dto.ErrorDTO.ErrorCode.INVALID_INPUT;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/cakes")
public class CakeController {
    private final static Logger LOG = LoggerFactory.getLogger(CakeController.class);

    private final CakeService cakeService;

    public CakeController(CakeService cakeService) {
        this.cakeService = cakeService;
    }

    @GetMapping(produces = {APPLICATION_JSON_VALUE})
    public List<CakeDTO> getAll() {
        return cakeService.findAll();
    }

    @PostMapping(produces = {APPLICATION_JSON_VALUE}, consumes = {APPLICATION_JSON_VALUE})
    public CakeDTO create(@Valid @RequestBody CakeDTO dto) {
        return cakeService.create(dto);
    }

    @ResponseStatus(BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ErrorDTO handleMethodArgumentNotValidException(MethodArgumentNotValidException ex) {
        Optional<String> errorFields = ex.getFieldErrors().stream()
                        .map(FieldError::getField)
                .reduce((field1, field2) -> field1 + ", " + field2);
        String errorMessage = errorFields.map(fields -> "Invalid input for field/s " + fields)
                .orElse("Invalid input");
        LOG.warn("Invalid cake object received {}", errorMessage, ex);

        return new ErrorDTO(INVALID_INPUT, errorMessage);
    }

    @ResponseStatus(BAD_REQUEST)
    @ExceptionHandler(DataIntegrityViolationException.class)
    public ErrorDTO handleDataIntegrityViolationException(DataIntegrityViolationException ex) {
        LOG.warn("Error when creating cake", ex);
        return new ErrorDTO(DUPLICATE_TITLE, "Cake title already exists");
    }
}

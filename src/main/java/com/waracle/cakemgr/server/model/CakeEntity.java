package com.waracle.cakemgr.server.model;

import javax.persistence.*;
import java.util.Objects;

@Entity(name = "cakes")
@Table(name = "cakes")
public class CakeEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false, updatable = false)
    private Long cakeId;

    @Column(name = "title", unique = true, nullable = false, length = 100)
    private String title;

    @Column(name = "description", length = 1500)
    private String description;

    @Column(name = "image_url", length = 1000)
    private String imageUrl;

    public Long getCakeId() {
        return cakeId;
    }

    public void setCakeId(Long cakeId) {
        this.cakeId = cakeId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CakeEntity)) return false;
        CakeEntity that = (CakeEntity) o;
        return Objects.equals(cakeId, that.cakeId) && Objects.equals(title, that.title) && Objects.equals(description, that.description) && Objects.equals(imageUrl, that.imageUrl);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cakeId, title, description, imageUrl);
    }
}
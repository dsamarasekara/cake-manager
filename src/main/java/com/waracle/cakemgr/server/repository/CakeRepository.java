package com.waracle.cakemgr.server.repository;

import com.waracle.cakemgr.server.model.CakeEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CakeRepository extends JpaRepository<CakeEntity, Long> {
}

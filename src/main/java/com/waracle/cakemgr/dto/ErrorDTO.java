package com.waracle.cakemgr.dto;

import java.util.StringJoiner;

public class ErrorDTO {
    private final ErrorCode errorCode;
    private final String message;

    public enum ErrorCode {
        INVALID_INPUT,
        DUPLICATE_TITLE
    }

    public ErrorDTO(ErrorCode errorCode, String message) {
        this.errorCode = errorCode;
        this.message = message;
    }

    public ErrorCode getErrorCode() {
        return errorCode;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", ErrorDTO.class.getSimpleName() + "[", "]")
                .add("errorCode=" + errorCode)
                .add("message='" + message + "'")
                .toString();
    }
}

package com.waracle.cakemgr.dto;

import org.hibernate.validator.constraints.URL;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Objects;
import java.util.StringJoiner;

public class CakeDTO {
    private Long cakeId;
    @NotBlank(message = "Title cannot be empty")
    @Size(max = 100)
    private String title;
    @Size(max = 1500)
    private String description;
    @URL
    @Size(max = 1000)
    private String imageUrl;

    public CakeDTO() {

    }

    public CakeDTO(Long cakeId, String title, String description, String imageUrl) {
        this.cakeId = cakeId;
        this.title = title;
        this.description = description;
        this.imageUrl = imageUrl;
    }

    public Long getCakeId() {
        return cakeId;
    }

    public void setCakeId(Long cakeId) {
        this.cakeId = cakeId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CakeDTO)) return false;
        CakeDTO cakeDTO = (CakeDTO) o;
        return Objects.equals(cakeId, cakeDTO.cakeId) && Objects.equals(title, cakeDTO.title) && Objects.equals(description, cakeDTO.description) && Objects.equals(imageUrl, cakeDTO.imageUrl);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cakeId, title, description, imageUrl);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", CakeDTO.class.getSimpleName() + "[", "]")
                .add("cakeId=" + cakeId)
                .add("title='" + title + "'")
                .add("description='" + description + "'")
                .add("imageUrl='" + imageUrl + "'")
                .toString();
    }
}

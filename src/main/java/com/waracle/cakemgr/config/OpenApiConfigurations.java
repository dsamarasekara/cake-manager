package com.waracle.cakemgr.config;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.context.annotation.Configuration;

@OpenAPIDefinition(info = @Info(title = "Cake Manager",
        description = "Waracle cake manager application", version = "v1"))
@Configuration
public class OpenApiConfigurations {
}

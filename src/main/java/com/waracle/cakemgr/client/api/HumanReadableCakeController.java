package com.waracle.cakemgr.client.api;

import com.waracle.cakemgr.client.service.CakeConvertor;
import com.waracle.cakemgr.client.service.CakeServiceClient;
import com.waracle.cakemgr.dto.CakeDTO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_FORM_URLENCODED_VALUE;
import static org.springframework.http.MediaType.TEXT_PLAIN_VALUE;

@RestController
public class HumanReadableCakeController {

    private final CakeServiceClient cakeService;

    public HumanReadableCakeController(CakeServiceClient cakeService) {
        this.cakeService = cakeService;
    }

    @Operation(summary = "Get all cakes")
    @GetMapping(produces = {TEXT_PLAIN_VALUE})
    public @ResponseBody
    String getAll() {
        List<CakeDTO> cakeDTOList = cakeService.getAllCakes();
        return CakeConvertor.convertToHumanReadable(cakeDTOList);
    }

    @Operation(summary = "Create a cakes", requestBody = @RequestBody(content = @Content(mediaType = APPLICATION_FORM_URLENCODED_VALUE)))
    @PostMapping(consumes = {APPLICATION_FORM_URLENCODED_VALUE})
    public void createCake(
            @RequestParam String title,
            @RequestParam(required = false) String description,
            @RequestParam(required = false) String imageUrl) {
        cakeService.createCake(title, description, imageUrl);
    }
}

package com.waracle.cakemgr.client.service;

import com.waracle.cakemgr.dto.CakeDTO;
import org.apache.logging.log4j.util.Strings;

import java.util.List;

public class CakeConvertor {
    private static final char COL_SEPARATOR = '|';

    public static String convertToHumanReadable(List<CakeDTO> cakeList) {
        List<String> cakeStringList = cakeList.stream().map(cakeDto -> new StringBuilder()
                        .append(cakeDto.getCakeId())
                        .append(COL_SEPARATOR)
                        .append(cakeDto.getTitle())
                        .append(COL_SEPARATOR)
                        .append(cakeDto.getDescription())
                        .append(COL_SEPARATOR)
                        .append(cakeDto.getImageUrl())
                        .toString())
                .toList();

        return "ID | Title | Description | Image URL\n" + Strings.join(cakeStringList, '\n');
    }
}

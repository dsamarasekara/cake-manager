package com.waracle.cakemgr.client.service;

public class CakeServiceException extends RuntimeException {

    public CakeServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    public CakeServiceException(String message) {
        super(message);
    }
}

package com.waracle.cakemgr.client.service;

import com.waracle.cakemgr.dto.CakeDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Component
@Slf4j
public class CakeServiceClient {

    private final RestTemplate restTemplate;

    private final String cakeServiceEndpoint;

    public CakeServiceClient(RestTemplate restTemplate,
                             @Value("${client.endpoint}") String cakeServiceEndpoint) {
        this.restTemplate = restTemplate;
        this.cakeServiceEndpoint = cakeServiceEndpoint;
    }

    public List<CakeDTO> getAllCakes() {
        try {
            MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
            headers.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
            headers.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
            HttpEntity<Void> entity = new HttpEntity<>(headers);

            ResponseEntity<List<CakeDTO>> cakeResponse = restTemplate.exchange(cakeServiceEndpoint,
                    HttpMethod.GET,
                    entity,
                    new ParameterizedTypeReference<List<CakeDTO>>() {
                    });

            if (cakeResponse.getStatusCode() == HttpStatus.OK) {
                return cakeResponse.getBody();
            }
            throw new CakeServiceException("Error occurred when getting all cakes. Response status - " + cakeResponse.getStatusCode());
        } catch (HttpClientErrorException e) {
            log.warn("Error occurred when getting all cakes", e);
            throw new CakeServiceException("Error occurred when getting all cakes. Response status - " + e.getStatusCode(), e);
        }
    }

    public void createCake(String title, String description, String imageUrl) {
        try {
            CakeDTO cakeDTO = new CakeDTO();
            cakeDTO.setTitle(title);
            cakeDTO.setDescription(description);
            cakeDTO.setImageUrl(imageUrl);

            MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
            headers.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
            HttpEntity<CakeDTO> entity = new HttpEntity<>(cakeDTO, headers);

            ResponseEntity<Void> cakeResponse = restTemplate.exchange(cakeServiceEndpoint,
                    HttpMethod.POST,
                    entity, Void.class);

            if (!cakeResponse.getStatusCode().is2xxSuccessful()) {
                throw new CakeServiceException("Error occurred when creating a new cake. Response status - " + cakeResponse.getStatusCode());
            }
        } catch (HttpClientErrorException e) {
            log.warn("Error occurred when creating a new cake", e);
            throw new CakeServiceException("Error occurred when creating a new cake. Response status - " + e.getStatusCode(), e);
        }
    }
}

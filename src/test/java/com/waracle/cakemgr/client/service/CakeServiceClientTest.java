package com.waracle.cakemgr.client.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.tomakehurst.wiremock.admin.model.ServeEventQuery;
import com.github.tomakehurst.wiremock.junit5.WireMockTest;
import com.github.tomakehurst.wiremock.stubbing.ServeEvent;
import com.waracle.cakemgr.dto.CakeDTO;
import org.junit.jupiter.api.Test;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.UUID;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.springframework.http.HttpHeaders.CONTENT_TYPE;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@WireMockTest(httpPort = 8081)
class CakeServiceClientTest {

    private final CakeServiceClient client;

    private final ObjectMapper objectMapper = new ObjectMapper();

    public CakeServiceClientTest() {
        this.client = new CakeServiceClient(new RestTemplate(), "http://localhost:8081/cakes");
    }

    @Test
    void shouldCreateCake() throws JsonProcessingException {
        // Given
        UUID stubId = stubFor(post("/cakes").willReturn(ok())).getId();

        // When
        client.createCake("t1", "d1", "i1");

        // Then
        List<ServeEvent> serveEvents =
                getAllServeEvents(ServeEventQuery.forStubMapping(stubId));
        CakeDTO request = objectMapper.readValue(serveEvents.get(0).getRequest().getBodyAsString(), CakeDTO.class);

        assertThat(request).isEqualTo(new CakeDTO(null, "t1", "d1", "i1"));
    }

    @Test
    void shouldThrowExceptionWhenCreateCakeReturnsError() {
        // Given
        UUID stubId = stubFor(post("/cakes").willReturn(badRequest())).getId();

        // Then
        assertThrows(CakeServiceException.class, () -> client.createCake("t1", "d1", "i1"));
    }

    @Test
    void shouldFindAllCakes() throws JsonProcessingException {
        // Given
        CakeDTO c1 = new CakeDTO(1L, "t1", null, null);
        CakeDTO c2 = new CakeDTO(2L, "t2", null, null);
        List<CakeDTO> cakeList = List.of(c1, c2);
        String responseBody = objectMapper.writeValueAsString(cakeList);
        stubFor(get("/cakes")
                .willReturn(ok()
                        .withHeader(CONTENT_TYPE, APPLICATION_JSON_VALUE)
                        .withBody(responseBody)));

        // When
        List<CakeDTO> result = client.getAllCakes();

        // Then
        assertThat(result).isEqualTo(cakeList);
    }

}
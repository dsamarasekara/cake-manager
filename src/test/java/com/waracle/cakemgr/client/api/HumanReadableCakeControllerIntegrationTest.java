package com.waracle.cakemgr.client.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.junit5.WireMockTest;
import com.waracle.cakemgr.dto.CakeDTO;
import org.apache.hc.client5.http.entity.UrlEncodedFormEntity;
import org.apache.hc.core5.http.io.entity.EntityUtils;
import org.apache.hc.core5.http.message.BasicNameValuePair;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import javax.transaction.Transactional;
import java.util.List;

import static com.github.tomakehurst.wiremock.client.WireMock.ok;
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import static org.springframework.http.HttpHeaders.CONTENT_TYPE;
import static org.springframework.http.MediaType.APPLICATION_FORM_URLENCODED_VALUE;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@Transactional
@WireMockTest(httpPort = 8081)
class HumanReadableCakeControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void shouldCreateCakeAndReturnStatus200() throws Exception {
        // Given
        String requestBody = EntityUtils.toString(new UrlEncodedFormEntity(List.of(
                new BasicNameValuePair("title", "t1"),
                new BasicNameValuePair("description", "d1"),
                new BasicNameValuePair("imageUrl", "http://image.com"))));
        stubFor(WireMock.post("/cakes").willReturn(ok()));

        // Then
        mockMvc.perform(post("/")
                        .contentType(APPLICATION_FORM_URLENCODED_VALUE)
                        .content(requestBody))
                .andExpect(status().isOk());
    }

    @Test
    void shouldReturnTextWhenFindAllCakesAndReturnStatus200() throws Exception {
        // Given
        CakeDTO c1 = new CakeDTO(1L, "t1", null, null);
        CakeDTO c2 = new CakeDTO(2L, "t2", null, null);
        List<CakeDTO> cakeList = List.of(c1, c2);
        String responseBody = objectMapper.writeValueAsString(cakeList);
        stubFor(WireMock.get("/cakes")
                .willReturn(ok()
                        .withHeader(CONTENT_TYPE, APPLICATION_JSON_VALUE)
                        .withBody(responseBody)));

        // Then
        mockMvc.perform(get("/")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("""
                        ID | Title | Description | Image URL
                        1|t1|null|null
                        2|t2|null|null"""));
    }

    @Test
    void shouldReturnTextHeadingWhenFindAllCakesIsEmpty() throws Exception {
        // Given
        stubFor(WireMock.get("/cakes")
                .willReturn(ok()
                        .withHeader(CONTENT_TYPE, APPLICATION_JSON_VALUE)
                        .withBody("[]")));

        // Then
        mockMvc.perform(get("/")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("ID | Title | Description | Image URL\n"));
    }

}
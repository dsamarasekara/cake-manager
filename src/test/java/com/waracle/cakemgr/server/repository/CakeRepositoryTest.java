package com.waracle.cakemgr.server.repository;

import com.waracle.cakemgr.server.model.CakeEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
class CakeRepositoryTest {

    @Autowired
    private CakeRepository cakeRepository;

    @Test
    void shouldSaveCake() {
        // Given
        CakeEntity c = createCakeEntity("t1", "d1", "imageUrl");

        // When
        CakeEntity savedC = cakeRepository.save(c);

        // Then
        CakeEntity result = cakeRepository.getById(savedC.getCakeId());
        assertThat(result).isEqualTo(c);
    }

    @Test
    void shouldSaveCakeWithOnlyTitle() {
        // Given
        CakeEntity c = createCakeEntity("t1", null, null);

        // When
        CakeEntity savedC = cakeRepository.save(c);

        // Then
        CakeEntity result = cakeRepository.getById(savedC.getCakeId());
        assertThat(result).isEqualTo(c);
    }

    @Test
    void shouldThrowExceptionWhenSaveWithDuplicateTitle() {
        // Given
        CakeEntity c1 = createCakeEntity("t1", "d1", "imageUrl");
        CakeEntity c2 = createCakeEntity("t1", "d1", "imageUrl");
        cakeRepository.save(c1);

        // When
        assertThrows(DataIntegrityViolationException.class, () -> cakeRepository.save(c2));
    }

    @Test
    void shouldThrowExceptionWhenSaveWithEmptyTitle() {
        // Given
        CakeEntity c1 = createCakeEntity(null, "d1", "imageUrl");

        // When
        assertThrows(DataIntegrityViolationException.class, () -> cakeRepository.save(c1));
    }

    @Test
    void shouldFindAllCakes() {
        // Given
        CakeEntity c1 = createCakeEntity("t1", "d1", "imageUrl");
        CakeEntity c2 = createCakeEntity("t2", "d1", "imageUrl");
        CakeEntity c3 = createCakeEntity("t3", "d1", "imageUrl");

        CakeEntity savedC1 = cakeRepository.save(c1);
        CakeEntity savedC2 = cakeRepository.save(c2);
        CakeEntity savedC3 = cakeRepository.save(c3);

        // When
        Page<CakeEntity> result = cakeRepository.findAll(PageRequest.of(0, 2));

        // Then
        assertThat(result.getTotalElements()).isEqualTo(3);
        assertThat(result.getNumberOfElements()).isEqualTo(2);
        assertThat(result.getTotalPages()).isEqualTo(2);
        assertThat(result.stream().toList()).isEqualTo(List.of(savedC1, savedC2));

        // When
        result = cakeRepository.findAll(PageRequest.of(1, 2));

        // Then
        assertThat(result.getNumberOfElements()).isEqualTo(1);
        assertThat(result.stream().toList()).isEqualTo(List.of(savedC3));
    }

    private CakeEntity createCakeEntity(String title, String description, String imageUrl) {
        CakeEntity c = new CakeEntity();
        c.setTitle(title);
        c.setDescription(description);
        c.setImageUrl(imageUrl);
        return c;
    }
}
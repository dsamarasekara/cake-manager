package com.waracle.cakemgr.server.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.waracle.cakemgr.dto.CakeDTO;
import com.waracle.cakemgr.server.model.CakeEntity;
import com.waracle.cakemgr.server.repository.CakeRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.IntStream;

import static com.waracle.cakemgr.dto.ErrorDTO.ErrorCode.DUPLICATE_TITLE;
import static com.waracle.cakemgr.dto.ErrorDTO.ErrorCode.INVALID_INPUT;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@Transactional
class CakeControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private CakeRepository cakeRepository;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void shouldCreateCakeAndReturnStatus200() throws Exception {
        // Given
        CakeDTO dto = createCakeDTO("t1", "d1", "http://image.com/1");
        String postRequestBody = objectMapper.writeValueAsString(dto);

        // Then
        mockMvc.perform(post("/cakes")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(postRequestBody))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.cakeId").isNumber())
                .andExpect(jsonPath("$.title").value("t1"))
                .andExpect(jsonPath("$.description").value("d1"))
                .andExpect(jsonPath("$.imageUrl").value("http://image.com/1"));

        List<CakeEntity> cakeList = cakeRepository.findAll();
        assertThat(cakeList).hasSize(1);
        assertThat(cakeList.get(0).getTitle()).isEqualTo("t1");
        assertThat(cakeList.get(0).getDescription()).isEqualTo("d1");
    }

    @Test
    void shouldFindAllCakesAndReturnStatus200() throws Exception {
        // Given
        createCake("t1", "d1", null);
        createCake("t2", "d2", null);

        // Then
        mockMvc.perform(get("/cakes")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(""))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$.[0].title").value("t1"))
                .andExpect(jsonPath("$.[0].description").value("d1"));
    }

    @Test
    void shouldReturnErrorWhenCreateCakeWithEmptyTitle() throws Exception {
        // Given
        CakeDTO dto = createCakeDTO("", "d1", "http://image.com/1");
        String postRequestBody = objectMapper.writeValueAsString(dto);

        // Then
        mockMvc.perform(post("/cakes")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(postRequestBody))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errorCode").value(INVALID_INPUT.name()))
                .andExpect(jsonPath("$.message").value("Invalid input for field/s title"));
    }

    @Test
    void shouldReturnErrorWhenCreateCakeWithLargeTitle() throws Exception {
        // Given
        String title = IntStream.range(0, 101)
                .mapToObj(String::valueOf)
                .reduce((a, b) -> a + b)
                .get();
        CakeDTO dto = createCakeDTO(title, "d1", "http://image.com/1");
        String postRequestBody = objectMapper.writeValueAsString(dto);

        // Then
        mockMvc.perform(post("/cakes")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(postRequestBody))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errorCode").value(INVALID_INPUT.name()))
                .andExpect(jsonPath("$.message").value("Invalid input for field/s title"));
    }

    @Test
    void shouldReturnErrorWhenCreateCakeWithDuplicateTitle() throws Exception {
        // Given
        createCake("t1", "d1", "http://image.com/1");
        CakeDTO dto = createCakeDTO("t1", "d2", "http://image.com/1");
        String postRequestBody = objectMapper.writeValueAsString(dto);

        // Then
        mockMvc.perform(post("/cakes")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(postRequestBody))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errorCode").value(DUPLICATE_TITLE.name()))
                .andExpect(jsonPath("$.message").value("Cake title already exists"));
    }

    private CakeDTO createCakeDTO(String title, String description, String imageUrl) {
        return new CakeDTO(null, title, description, imageUrl);
    }

    private void createCake(String title, String description, String imageUrl) {
        CakeEntity cake = new CakeEntity();
        cake.setTitle(title);
        cake.setDescription(description);
        cake.setImageUrl(imageUrl);
        cakeRepository.save(cake);
    }
}
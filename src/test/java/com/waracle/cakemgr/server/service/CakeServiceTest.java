package com.waracle.cakemgr.server.service;

import com.waracle.cakemgr.dto.CakeDTO;
import com.waracle.cakemgr.server.model.CakeEntity;
import com.waracle.cakemgr.server.repository.CakeRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static java.util.Collections.emptyList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
class CakeServiceTest {
    @Mock
    private CakeRepository cakeRepository;

    @InjectMocks
    private CakeService cakeService;

    @Test
    void shouldCreateCake() {
        // Given
        CakeDTO dto = new CakeDTO(null, "t1", "d1", "http://image.com");

        CakeEntity c = createCakeEntity(1L, "t1", "d1", "http://image.com");

        final ArgumentCaptor<CakeEntity> cakeEntityCaptor = ArgumentCaptor.forClass(CakeEntity.class);
        given(cakeRepository.save(cakeEntityCaptor.capture()))
                .willReturn(c);

        // When
        CakeDTO result = cakeService.create(dto);

        // Then
        assertThat(result.getCakeId()).isEqualTo(1L);
        assertThat(result.getTitle()).isEqualTo("t1");
        assertThat(result.getDescription()).isEqualTo("d1");
        assertThat(result.getImageUrl()).isEqualTo("http://image.com");

        assertThat(cakeEntityCaptor.getValue().getTitle()).isEqualTo("t1");
        assertThat(cakeEntityCaptor.getValue().getDescription()).isEqualTo("d1");
        assertThat(cakeEntityCaptor.getValue().getCakeId()).isNull();
        assertThat(cakeEntityCaptor.getValue().getImageUrl()).isEqualTo("http://image.com");
    }

    @Test
    void shouldFindCakes() {
        // Given
        CakeEntity c1 = createCakeEntity(1L, "t1", "d1", "http://image.com");
        CakeEntity c2 = createCakeEntity(2L, "t2", "d2", "http://image.com");

        CakeDTO dto1 = new CakeDTO(1L, "t1", "d1", "http://image.com");
        CakeDTO dto2 = new CakeDTO(2L, "t2", "d2", "http://image.com");

        given(cakeRepository.findAll())
                .willReturn(List.of(c1, c2));

        // When
        List<CakeDTO> result = cakeService.findAll();

        // Then
        assertThat(result.size()).isEqualTo(2);
        assertThat(result).isEqualTo(List.of(dto1, dto2));
    }

    @Test
    void shouldReturnEmptyPageWhenNoCakesAvailable() {
        // Given
        given(cakeRepository.findAll())
                .willReturn(emptyList());

        // When
        List<CakeDTO> result = cakeService.findAll();

        // Then
        assertThat(result.size()).isEqualTo(0);
    }

    private CakeEntity createCakeEntity(Long cakeId, String title, String description, String imageUrl) {
        CakeEntity c = new CakeEntity();
        c.setCakeId(cakeId);
        c.setTitle(title);
        c.setDescription(description);
        c.setImageUrl(imageUrl);
        return c;
    }
}